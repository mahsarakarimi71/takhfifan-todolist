import { Todo } from "../../types/todo";
import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export const todosApi = createApi({
  reducerPath: "todosApi",
  refetchOnFocus: false,
  baseQuery: fetchBaseQuery({
    baseUrl: "https://64d0ed78ff953154bb79ba72.mockapi.io",
  }),

  endpoints: (builder) => ({
    getTodos: builder.query<Todo[], void>({
      query: () => {
        const params = new URLSearchParams({
          // page: page.toString(),
          page: "1",
          // limit: "10",
        });
        return `/todos?${params}`;
      },
    }),
    getTodo: builder.query<Todo, string>({
      query: (id) => `/todos/${id}`,
    }),
    addTodo: builder.mutation<Todo, Partial<Todo>>({
      query: (body) => ({
        url: "/todos",
        method: "POST",
        body,
      }),
    }),
    updateTodo: builder.mutation<void, Pick<Todo, "id"> & Partial<Todo>>({
      query: ({ id, ...patch }) => ({
        url: `/todos/${id}`,
        method: "PUT",
        body: patch,
      }),
      async onQueryStarted({ id, ...patch }, { dispatch, queryFulfilled }) {
        const patchResult = dispatch(
          todosApi.util.updateQueryData("getTodo", id, (draft) => {
            Object.assign(draft, patch);
          })
        );
        try {
          await queryFulfilled;
        } catch {
          patchResult.undo();
        }
      },
    }),
    deleteTodo: builder.mutation<{ success: boolean; id: number }, number>({
      query(id) {
        return {
          url: `/todos/${id}`,
          method: "DELETE",
        };
      },
    }),
  }),
});

export const {
  useGetTodosQuery,
  useGetTodoQuery,
  useAddTodoMutation,
  useUpdateTodoMutation,
  useDeleteTodoMutation,
} = todosApi;
