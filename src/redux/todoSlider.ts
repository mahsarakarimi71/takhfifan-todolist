import { createSlice } from "@reduxjs/toolkit";
export const toDoSlider = createSlice({
  name: "toDo",
  initialState: {
    todo: {
      title: "",
      description: "",
      status: false,
      id: "",
    },
  },
  reducers: {
    editTodo: (state, action) => {
      state.todo = action.payload.item;
    },
  },
});

export const { editTodo } = toDoSlider.actions;
export default toDoSlider.reducer;
