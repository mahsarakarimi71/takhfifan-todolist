import { useState } from "react";
import "./App.css";
import { TodoList } from "./components";

function App() {
  const [visible, setVisible] = useState(false);

  const toggleModal = () => {
    setVisible(!visible);
  };

  return (
    <div className="appMain">
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <h1>Task Manager</h1>
        <button className="btnAdd btnAddNew" onClick={toggleModal}>
          Add New
        </button>
      </div>
      <TodoList visible={visible} toggleModal={toggleModal} />
    </div>
  );
}

export default App;
