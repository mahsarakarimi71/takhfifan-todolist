import { ReactElement } from "react";

export type GeneralType = {
  children: ReactElement | ReactElement[];
};
