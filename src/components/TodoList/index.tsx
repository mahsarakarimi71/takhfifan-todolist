import { useGetTodosQuery } from "../../redux/services/todosApi";
import { AddTodo } from "../AddTodo";
import { Todo } from "../Todo";

type TodoListProps = {
  visible: boolean;
  toggleModal: () => void;
};

export const TodoList = ({ visible, toggleModal }: TodoListProps) => {
  const { data, isLoading, refetch } = useGetTodosQuery();

  if (isLoading) {
    return null;
  }

  const onSuccess = () => {
    refetch();
  };

  return (
    <div className="App">
      <AddTodo onSuccess={onSuccess} visible={visible} onClose={toggleModal} />
      <div>
        {data &&
          data.length &&
          data.map((item, index) => (
            <Todo
              item={item}
              key={index}
              onSuccess={onSuccess}
              toggleModal={toggleModal}
            />
          ))}
      </div>
    </div>
  );
};
