import React from "react";
import "./card.css";
import { GeneralType } from "../../types";
import { CardFooter } from "../CardFooter";

type CardProps = React.ComponentProps<"div"> & {
  customType?: string;
};
type CardFooterProps = {
  deleteItem: () => void;
  editItem: () => void;
  isDeleting: boolean;
};

export const Card = ({ children, customType, ...restProps }: CardProps) => {
  return (
    <div
      className={customType && customType === "add" ? "mainAdd" : "main"}
      {...restProps}
    >
      {children}
    </div>
  );
};

const Header = ({ children, ...restProps }: GeneralType) => {
  return (
    <div {...restProps} className="header">
      {children}
    </div>
  );
};

const Body = ({ children, ...restProps }: GeneralType) => {
  return <div {...restProps}>{children}</div>;
};

const Footer = ({ deleteItem, editItem, isDeleting }: CardFooterProps) => {
  return (
    <div className="footer">
      <CardFooter
        deleteItem={deleteItem}
        editItem={editItem}
        isDeleting={isDeleting}
      />
    </div>
  );
};

Card.Header = Header;
Card.Body = Body;
Card.Footer = Footer;
