import {
  useDeleteTodoMutation,
  useUpdateTodoMutation,
} from "../../redux/services/todosApi";
import { editTodo } from "../../redux/todoSlider";
import { Todo as TodoType } from "../../types/todo";
import { Card } from "../Card";
import { useAppDispatch } from "../../redux/store";
import { useEffect, useState } from "react";

type TodoProps = {
  item: TodoType;
  key: any;
  onSuccess: () => void;
  toggleModal: () => void;
};

export const Todo = ({ item, key, onSuccess, toggleModal }: TodoProps) => {
  const [deleteTodo, { isLoading: isDeleting }] = useDeleteTodoMutation();
  const [updateTodo] = useUpdateTodoMutation();
  const [isCheck, setisCheck] = useState(false);
  const dispatch = useAppDispatch();

  const UpdateTodoo = () => {
    const id = item.id;
    dispatch(editTodo({ item, id }));
    toggleModal();
  };

  const DeleteTodo = () => {
    const id = parseInt(item.id);
    deleteTodo(id).then(() => {
      onSuccess();
    });
  };

  const onChangeCheckBox = () => {
    setisCheck(!isCheck);
    const data = { ...item, status: !isCheck };
    console.log(data);
    updateTodo(data).then(() => {
      onSuccess();
    });
  };

  useEffect(() => {
    setisCheck(item.status);
  }, []);

  return (
    <Card key={key}>
      <Card.Header>
        <h2>{item.title}</h2>
        <span>{item.description}</span>
        <div style={{ marginTop: "20px" }}>
          <label className="labelCheckBox">
            {item.status ? "Compeleted" : "Doing"}
            <input
              type="checkbox"
              defaultChecked={item.status || isCheck ? true : false}
              onChange={onChangeCheckBox}
            />
          </label>
        </div>
      </Card.Header>
      <Card.Footer
        deleteItem={DeleteTodo}
        editItem={UpdateTodoo}
        isDeleting={isDeleting}
      />
    </Card>
  );
};
