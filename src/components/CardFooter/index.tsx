import "./cardFooter.css";

type CardFooterProps = {
  deleteItem: () => void;
  editItem: () => void;
  isDeleting: boolean;
};

export const CardFooter = ({
  deleteItem,
  editItem,
  isDeleting,
}: CardFooterProps) => {
  return (
    <div className="mainFooter">
      <div className="divButton">
        <button className="btns btnEdit" onClick={editItem}>
          Edit
        </button>
      </div>
      <div className="divButton">
        <button
          className="btns btnDelete"
          onClick={deleteItem}
          disabled={isDeleting}
        >
          Delete
        </button>
      </div>
    </div>
  );
};
