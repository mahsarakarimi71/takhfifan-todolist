import "./addTodo.css";
import { AddTodofrom } from "../AddTodoForm";
import { Card } from "../Card";
import {
  useAddTodoMutation,
  useUpdateTodoMutation,
} from "../../redux/services/todosApi";
import { Todo } from "../../types/todo";
import { createPortal } from "react-dom";

type AddTodoProps = {
  onSuccess: () => void;
  onClose: () => void;
  visible: boolean;
};

export const AddTodo = ({ onSuccess, visible, onClose }: AddTodoProps) => {
  const [addTodo, { isLoading }] = useAddTodoMutation();
  const [updateTodo, { isLoading: isUpdating }] = useUpdateTodoMutation();

  const handleSubmit = (formData: Todo) => {
    if (formData.id) {
      updateTodo(formData).then(() => {
        onSuccess();
        onClose();
      });
    } else {
      addTodo(formData).then(() => {
        onSuccess();
        onClose();
      });
    }
  };

  if (!visible) {
    return null;
  }

  return createPortal(
    <div
      style={{
        position: "fixed",
        width: "100vw",
        height: "100vh",
        backgroundColor: "#000000af",
        zIndex: 100,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        top: 0,
      }}
    >
      <Card customType="add">
        <Card.Body>
          <div className="newHeader">
            <div className="divHeaderTitle">
              <h4>Details</h4>
            </div>
          </div>
        </Card.Body>
        <Card.Body>
          <div className="divBody">
            <AddTodofrom
              onSubmit={handleSubmit}
              isLoading={isLoading || isUpdating}
              closeModal={onClose}
            />
          </div>
        </Card.Body>
      </Card>
    </div>,
    document.body
  );
};
