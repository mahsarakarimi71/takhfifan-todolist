import "./AddTodoForm.css";
import { useForm } from "react-hook-form";
import { Loading } from "../Loading";
import { useAppSelector } from "../../redux/store";
import { Todo } from "../../types/todo";
import { useEffect } from "react";

type AddTodoFormProps = {
  isLoading: boolean;
  onSubmit: (data: Todo) => void;
  closeModal: () => void;
};

export const AddTodofrom = ({
  isLoading,
  onSubmit,
  closeModal,
}: AddTodoFormProps) => {
  const editTodo = useAppSelector((state) => state.toDo);

  const { register, handleSubmit, reset } = useForm<Todo>({
    defaultValues: {
      title: editTodo.todo.title,
      description: editTodo.todo.description,
      status: editTodo.todo.status,
    },
  });

  useEffect(() => {
    if (editTodo.todo.id) {
      reset(editTodo.todo);
    }
  }, [editTodo.todo]);

  const submit = handleSubmit((data) => {
    onSubmit(data);
    reset({
      title: "",
      description: "",
      status: false,
      id: "",
    });
  });

  return (
    <form onSubmit={submit}>
      <label className="label">
        Title
        <input
          type="text"
          placeholder="Title"
          className="input"
          {...register("title")}
        />
      </label>
      <label className="label">
        Description
        <input
          type="text"
          placeholder="Description"
          className="input"
          {...register("description")}
        />
      </label>
      <label className="labelCheckBox">
        Compeleted <input type="checkbox" {...register("status")} />
      </label>
      <div className="btnsDiv">
        <button
          type="submit"
          className="btnHeader btnHeaderNew"
          disabled={isLoading}
        >
          {isLoading ? <Loading /> : "Submit"}
        </button>
        <button className="btnHeader btnHeaderNew" onClick={closeModal}>
          Close
        </button>
      </div>
    </form>
  );
};
